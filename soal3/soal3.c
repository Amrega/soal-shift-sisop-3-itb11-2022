#include <ctype.h>
#include <dirent.h>
#include <pthread.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <unistd.h>

int dir()
{    
    pthread_t tid[1];
    int totaldir = 0;
    const char *folders[] = {"bin","c","fbx","gif","gns3project","hex","Hidden","jpg","jpeg","js","pdf","png","tar.gz","txt","Unknown","zip"};
    const char *extras[] = {"Hidden","Unknown"};

    while (totaldir<=15)
    {
       mkdir(folders[totaldir], S_IRWXU);
       mkdir(extras[totaldir], S_IRWXU);
       totaldir++;
    }
    return 0;
}

int main()
{
    dir();
}